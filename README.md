# Install Anaconda 

## Select the latest Anaconda installer:

[https://repo.anaconda.com/archive/](https://repo.anaconda.com/archive/)
    
    e.g: Anaconda3-2020.02-Linux-x86_64.sh

**Run:**
    `wget https://repo.anaconda.com/archive/Anaconda3-2020.02-Linux-x86_64.sh`

Anaconda3-2020.02-Linux-x86_64.sh - file will appear in your directory (run: ls to check)
    

## install Ananconda:

**Run:**
    `bash Anaconda3-2019.03-Linux-x86_64.sh`

anaonda3 will appear in your directory


# Connect Jupyter Notebook from local server to remote server (realvantage server)


------------------------------------------ For Windows user (Putty users) --------------------------------------------------------------

**Switch on** PuTTY:
    **Go to:** SSH -> Auth -> Tunnels


**Enter the following**

Depending on your jupyter note book local host- Mine is: http://localhost:8888/tree

1. Source port: 8888
2. Destination: localhost:8888
3. **Click** Add

**Open** rvserver

**Run:** `jupyter notebook --no-browser --port=8888`

Ubuntu will return:

 To access the notebook, open this file in a browser:
        file:///home/qixiang/.local/share/jupyter/runtime/nbserver-17480-open.html
    Or copy and paste one of these URLs:
        http://localhost:8888/?token=16456576bdb941ddd35594d5ea8e899bbaf371e16e95a14d
     or http://127.0.0.1:8888/?token=16456576bdb941ddd35594d5ea8e899bbaf371e16e95a14d

**Copy and paste** : either 'http://localhost:8888/' or 'http://127.0.0.1:8888/'

You will be required to submit a token:

token = 16456576bdb941ddd35594d5ea8e899bbaf371e16e95a14d

**Enter** the token to access Jupyter notebook

token will change for every access.

----------- or------------

copy and paste one of these URLs:

1. http://localhost:8888/?token=16456576bdb941ddd35594d5ea8e899bbaf371e16e95a14d
2. http://127.0.0.1:8888/?token=16456576bdb941ddd35594d5ea8e899bbaf371e16e95a14d

--------------------------------------------------------------For Mac users --------------------------------------------------------------------------------------------------------

You can try running a direct connection through your command line:

**Run:**
    `ssh -N -f -L localhost:8888:localhost:8888 qixiang@rvserver-ssh.realvantage.co`

**Go to:** RV server (Ubuntu)

**Run:** `jupyter notebook --no-browser --port=8888`

Ubuntu will return:

 To access the notebook, open this file in a browser:
        file:///home/qixiang/.local/share/jupyter/runtime/nbserver-17480-open.html
    Or copy and paste one of these URLs:
        http://localhost:8888/?token=16456576bdb941ddd35594d5ea8e899bbaf371e16e95a14d
     or http://127.0.0.1:8888/?token=16456576bdb941ddd35594d5ea8e899bbaf371e16e95a14d

**Copy and paste** : either 'http://localhost:8888/' or 'http://127.0.0.1:8888/'

You will be required to submit a token:

token = 16456576bdb941ddd35594d5ea8e899bbaf371e16e95a14d

**Enter** the token to access Jupyter notebook

token will change for every access.

----------- or------------

copy and paste one of these URLs:

1. http://localhost:8888/?token=16456576bdb941ddd35594d5ea8e899bbaf371e16e95a14d
2. http://127.0.0.1:8888/?token=16456576bdb941ddd35594d5ea8e899bbaf371e16e95a14d

## References


1. [Install anaconda](https://www.digitalocean.com/community/tutorials/how-to-install-anaconda-on-ubuntu-18-04-quickstart)
2. [Connect local jupyter to remote jupter](https://ljvmiranda921.github.io/notebook/2018/01/31/running-a-jupyter-notebook/)
3. [Anaconda Installer](https://repo.anaconda.com/archive/)
















